package com.karavatskiy.serhii.sample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.karavatskiy.serhii.sample.AdapterRecyclerView.OnItemClickListener;

/**
 * Created by Serhii on 08.02.2019.
 */
public class ViewHolderMessage extends ViewHolder {

    @BindView(R.id.rlMainContainer)
    View layout;

    @BindView(R.id.ivIconMan)
    ImageView ivIconMan;

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @BindView(R.id.tvData)
    TextView tvData;

    public ViewHolderMessage(@NonNull final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(DefaultMessage defaultMessage, OnItemClickListener onItemClickListener) {
        if (defaultMessage.getSender() == 0) {
            layout.setOnClickListener(v -> {
                onItemClickListener.onItemClick(defaultMessage.getMessage(), 100);
            });
        }
        ivIconMan.setImageResource(defaultMessage.getIvPath());
        tvMessage.setText(defaultMessage.getMessage());
        tvData.setText(defaultMessage.getData());
    }
}
