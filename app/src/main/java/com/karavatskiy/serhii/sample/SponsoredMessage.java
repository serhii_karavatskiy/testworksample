package com.karavatskiy.serhii.sample;

/**
 * Created by Serhii on 08.02.2019.
 */
public class SponsoredMessage extends DefaultMessage {

    private String header;

    private String textMessage;

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setHeader(final String header) {
        this.header = header;
    }

    @Override
    public String getMessage() {
        return textMessage;
    }

    @Override
    public void setMessage(final String textMessage) {
        this.textMessage = textMessage;
    }

    @Override
    public int getSender() {
        return 2;
    }

}
