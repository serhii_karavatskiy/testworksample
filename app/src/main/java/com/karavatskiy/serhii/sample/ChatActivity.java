package com.karavatskiy.serhii.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class ChatActivity extends AppCompatActivity implements RewardedVideoAdListener {


    @BindView(R.id.rvChat)
    RecyclerView rvChat;
    @BindView(R.id.toolBar)
    Toolbar toolbar;

    private RewardedVideoAd mRewardedVideoAd;

    private AdapterRecyclerView adapterRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        toolbar.setTitle(getString(R.string.title));
        setSupportActionBar(toolbar);

        MobileAds.initialize(this, getString(R.string.admob_key));
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        adapterRecyclerView = new AdapterRecyclerView((text, requestCode) -> {
            if (requestCode == 100) {
                Toast.makeText(ChatActivity.this, text, Toast.LENGTH_SHORT).show();
            } else if (requestCode == 101) {
                loadRewardedVideoAd();
            }
        });
        rvChat.setAdapter(adapterRecyclerView);
        rvChat.setLayoutManager(new LinearLayoutManager(this));
        adapterRecyclerView.setItems(CreateMock.initMock());
        addSponsor();
    }

    private void addSponsor() {
        SponsoredMessage sponsoredMessage = new SponsoredMessage();
        sponsoredMessage.setHeader(getString(R.string.text_sponsor));
        sponsoredMessage.setMessage(getString(R.string.text_watch_ad));
        adapterRecyclerView.addItem(sponsoredMessage);
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd(getString(R.string.admod_reward_key),
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        mRewardedVideoAd.show();
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(final RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(final int i) {

    }

    @Override
    public void onRewardedVideoCompleted() {

    }

    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }
}
