package com.karavatskiy.serhii.sample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.karavatskiy.serhii.sample.AdapterRecyclerView.OnItemClickListener;

/**
 * Created by Serhii on 08.02.2019.
 */
public class ViewHolderSponsor extends ViewHolder {

    @BindView(R.id.btnShowReward)
    Button btnShowReward;

    @BindView(R.id.tvSponsor)
    TextView tvSponsor;

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    public ViewHolderSponsor(@NonNull final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(DefaultMessage defaultMessage, OnItemClickListener onItemClickListener) {
        if (defaultMessage.getSender() == 2) {
            btnShowReward.setOnClickListener(v -> {
                onItemClickListener.onItemClick(null, 101);
            });
        }
        tvMessage.setText(defaultMessage.getMessage());
        tvSponsor.setText(defaultMessage.getHeader());
    }
}
