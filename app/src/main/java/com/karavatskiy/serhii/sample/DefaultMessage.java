package com.karavatskiy.serhii.sample;

/**
 * Created by Serhii on 08.02.2019.
 */
public class DefaultMessage {

    private int ivPath;
    private String message;
    private String data;
    private String header;
    private int sender;

    public int getIvPath() {
        return ivPath;
    }

    public void setIvPath(final int ivPath) {
        this.ivPath = ivPath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = data;
    }

    public int getSender() {
        return sender;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(final String header) {
        this.header = header;
    }

    public void setSender(final int sender) {
        this.sender = sender;
    }
}
