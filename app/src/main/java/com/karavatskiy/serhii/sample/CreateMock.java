package com.karavatskiy.serhii.sample;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Serhii on 08.02.2019.
 */
public class CreateMock {

    public static List<DefaultMessage> initMock() {
        List<DefaultMessage> messages = new ArrayList<>();
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String date = fmt.format(Calendar.getInstance().getTime());
        for (int i = 0; i < 10; i++) {
            DefaultMessage temp = new DefaultMessage();
            temp.setData(date);
            if (i % 2 == 0) {
                temp.setIvPath(R.drawable.a_man);
                temp.setMessage("Hi man its Luke");
                temp.setSender(0);
            } else {
                temp.setIvPath(R.drawable.b_man);
                temp.setMessage("Hi Luke, im ur father");
                temp.setSender(1);
            }
            messages.add(temp);
        }
        return messages;
    }
}
