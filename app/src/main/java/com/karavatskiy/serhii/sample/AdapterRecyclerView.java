package com.karavatskiy.serhii.sample;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Serhii on 08.02.2019.
 */
public class AdapterRecyclerView extends Adapter<ViewHolder> {

    @IntDef({MessageType.ME, MessageType.YOU})
    private @interface MessageType {

        int ME = 0;
        int YOU = 1;
        int SPONSOR = 2;
    }

    private List<DefaultMessage> items;

    private OnItemClickListener onItemClickListener;

    AdapterRecyclerView(OnItemClickListener onItemClickListener) {
        items = Collections.emptyList();
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(SponsoredMessage item) {
        if (items.isEmpty()) {
            this.items = new ArrayList<>();
            items.add(item);
        } else {
            items.add(item);
        }
        notifyDataSetChanged();
    }

    public void setItems(List<DefaultMessage> items) {
        this.items = new ArrayList<>();
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    @Override
    public int getItemViewType(int position) {
        switch (items.get(position)
                .getSender()) {
            case MessageType.ME:
                return MessageType.ME;
            case MessageType.YOU:
                return MessageType.YOU;
            case MessageType.SPONSOR:
                return MessageType.SPONSOR;
            default:
                return MessageType.ME;
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case MessageType.ME:
                viewHolder = new ViewHolderMessage(inflater.inflate(R.layout.layout_holder_me,
                        viewGroup,
                        false));
                break;
            case MessageType.YOU:
                viewHolder = new ViewHolderMessage(inflater.inflate(R.layout.layout_holder_you,
                        viewGroup,
                        false));
                break;
            case MessageType.SPONSOR:
                viewHolder = new ViewHolderSponsor(inflater.inflate(R.layout.layout_holder_sponsored,
                        viewGroup,
                        false));
                break;
            default:
                viewHolder = new ViewHolderMessage(inflater.inflate(R.layout.layout_holder_me,
                        viewGroup,
                        false));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        switch (viewHolder.getItemViewType()) {
            case MessageType.ME:
            case MessageType.YOU:
                ViewHolderMessage viewHolderMessage = (ViewHolderMessage) viewHolder;
                viewHolderMessage.bind(items.get(i), onItemClickListener);
                break;
            case MessageType.SPONSOR:
                ViewHolderSponsor viewHolderSponsor = (ViewHolderSponsor) viewHolder;
                viewHolderSponsor.bind(items.get(i), onItemClickListener);
                break;
        }
    }


    public interface OnItemClickListener {
        void onItemClick(@Nullable String text, int requestCode);
    }
}

